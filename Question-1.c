#include <stdio.h>
#define SIZE 1000

int main()
{
  char str[SIZE], rev[SIZE];
  int i, j, count = 0;
  printf("String Before Reverse: \n", str);
  fgets(str,SIZE,stdin);
  //finding the length of the string
  while (str[count] != '\0')
  {
    count++;
  }
  j = count - 1;

  //reversing the string by swapping
  for (i = 0; i < count; i++)
  {
    rev[i] = str[j];
    j--;
  }

  printf("String After Reverse: %s", rev);
  return 0;
}

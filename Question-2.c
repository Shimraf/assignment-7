#include <stdio.h>

int  main(){
   char str[1000],charact;
   int i,count=0;

	printf("Input the string : ");
    fgets(str,sizeof str,stdin);

   printf("Input the character to find frequency: ");
   scanf("%c",&charact);
   for(i=0;str[i]!='\0';++i)
   {
       if(charact==str[i])
           ++count;
   }
   printf("The frequency of '%c' is : %d\n\n", charact, count);
   return 0;
}
